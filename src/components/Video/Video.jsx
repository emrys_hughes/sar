import React from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';
/*import VimeoPlayer from 'react-player/lib/players/Vimeo';*/

import FontAwesome from 'react-fontawesome';

export default class Video extends React.Component {
    
    constructor(props) {
      super(props);
      this.state = { stage: 'intro' }
    }
    
    showVideo = () => {
        this.setState({stage: 'video'})
    }

    render() {
        const stage = this.state.stage==='intro'; 
        return (
             stage ?
               ( <div className="intro">
                    <div className="VideoIntro" dangerouslySetInnerHTML={{ __html: this.props.videoIntroText }}></div>  
                    <button onClick={() => this.showVideo()}>
                        <FontAwesome name="play" size="2x" />
                    </button>
                </div>
              ) : (
                <div className="player-wrapper">
                    <ReactPlayer 
                        url={'https://player.vimeo.com/video/' + this.props.vimeoCode } className="react-player"
                        width='100%'
                        height='100%' 
                        onEnded={ (e) => this.props.onChange(e,1)}
                    />
                </div>
              )
            
        )

    }
}

Video.propTypes = {
    videoIntroText: PropTypes.string,
    vimeoCode: PropTypes.string,
    step: PropTypes.number, // which step of the course
    onChange: PropTypes.any,  // function to move forward/backward through the course: we use this when the video ends
};
